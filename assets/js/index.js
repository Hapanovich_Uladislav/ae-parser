$(document).ready(function () {
    let socket = io.connect()
        , total = 0
        , processed = 0
        , popupIsVisible = false
        , countOfLinks = 1;

    $('#parser').on('submit', function (event) {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/',
            data: $(this).serialize()
        });
    });

    $('#progress').on('click', function() {
        swal({
            title: 'Процесс обработки продуктов',
            text: getText(),
            confirmButtonColor: '#2196F3',
            confirmButtonText: 'Скрыть'
        });
    });

    $('.confirm').on('click', function() {
        popupIsVisible = false;
    });

    $('#new-link').on('click', function () {
        if (countOfLinks < 10) {
            countOfLinks++;

            $('#links-container').append(`
            <div class="col-lg-12">
                <div class="col-md-11">
                    <div class="form-group">
                        <input class="form-control" placeholder="Сслыка на результат" name="uri[]">
                    </div>
                </div>
                
                <div class="col-md-1">
                    <div class="form-group">
                        <button type="button" class="btn btn-danger btn-lg legitRipple remove" style="padding: 8px 15px;">
                            <i class="icon-diff-removed"></i>
                        </button>
                    </div>
                </div>
            </div>
        `);
        }
    });

    $(document).on('click', '.remove', function() {
        $(this).parent().parent().parent().remove();
    });

    let showProgressModal = function() {
        if (popupIsVisible) {
            $('div.sweet-alert.showSweetAlert.visible > p').text(getText());
        } else {
            swal({
                title: 'Процесс обработки продуктов',
                text: getText(),
                confirmButtonColor: '#2196F3',
                confirmButtonText: 'Скрыть'
            });

            popupIsVisible = true;
        }
    };

    let getText = function() {
        let text = 'Начался поиск продуктов';

        if (processed > 0) {
            text = `Обработано ${processed} из ${total}`;
        } else if (total > 0) {
            text = `Начинается обработка ${total} продуктов`;
        }

        return text;
    };

    socket.on('connect', () => {
        socket.emit('join', socket.id);
    });

    socket.on('file', data => {
        $('#inputFileName').val(data);
        $('#file').submit();
    });

    socket.on('start', () => {
        console.log('start')
        $('#progress').css('display', 'block');

        showProgressModal();
    });

    socket.on('links', data => {
        total = data;
        processed = 0;

        $('#progress').css('display', 'block');


        showProgressModal();
    });

    socket.on('process', data => {
        processed += data;

        if (popupIsVisible) {
            showProgressModal()
        }
    });

    socket.on('finish', data => {
        processed += data;

        swal({
            title: 'Обработка завершена',
            text: `Продуктов обработано: ${processed}`,
            confirmButtonColor: '#66BB6A',
            type: 'success'
        });

        processed = 0;
        popupIsVisible = false;

        $('#progress').css('display', 'none');
    });
})