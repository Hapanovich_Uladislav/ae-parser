module.exports = {
    getPurchaseHistoryUrl(productId, page = 1) {
        return `https://feedback.aliexpress.com/display/evaluationProductDetailAjaxService.htm?productId=${productId}&type=default&page=${page}&_=${Date.now()}`
    }
};