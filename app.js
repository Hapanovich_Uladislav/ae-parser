require('dotenv').config();

const PORT          = process.env.PORT
    , USERNAME      = process.env.USERNAME
    , PASSWORD      = process.env.PASSWORD
    , express       = require('express')
    , app           = express()
    , bodyParser    = require('body-parser')
    , cookieParser = require('cookie-parser')
    , fs            = require('fs')
    , request       = require('request')
    , cheerio       = require('cheerio')
    , urls          = require('./app/data/urls')
    , headers       = require('./app/data/headers')
    , _             = require('lodash')
    , path          = require('path')
    , moment        = require('moment')
    , https         = require('follow-redirects/https')
    , delay         = require('delay');

let server = require('follow-redirects/http').createServer(app);

const TIME_TO_PROCESS_ONE_PRODUCT_IN_MILLISECONDS = 3000;

if (process.env.ENV !== 'local') {
    const options = {
        key: fs.readFileSync('../../../etc/letsencrypt/live/bot-host.com/privkey.pem'),
        cert: fs.readFileSync('../../../etc/letsencrypt/live/bot-host.com/cert.pem')
    };

    server = https.createServer(options, app);
}

const io = require('socket.io').listen(server);

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'node_modules')));
app.use(express.static(path.join(__dirname, 'assets')));
app.use(express.static(path.join(__dirname, 'results')));

app.route('/getFile')
    .post((request, response) => {
        response.download(request.body.fileName);
    });

app.route('/')
    .get((request, response) => {
        checkUser(request.cookies.user)
            ? response.sendFile('index.html', { root: './' })
            : response.redirect('/login');

    })
    .post(async (request, response) => {
        if (request.body.uri && checkUser(request.cookies.user)) {
            request.body.uri = request.body.uri.filter(link => link);

            if (request.body.uri.length > 0) {
                await getAds(request.body, request.cookies.io, 0);
            }
        } else {
            response.sendFile('index.html', { root: './' });
        }
    });

app.route('/login')
    .get((request, response) => response.sendFile('login.html', { root: './' }))
    .post((request, response) => {
        if (isTrueUser(request.body.user)) {
            response.cookie('user',request.body.user, { maxAge: 9000000 });

            response.redirect('/');
        } else {
            response.redirect('/login');
        }
    });

app.route('/logout')
    .get((request, response) => {
        response.clearCookie('user');

        response.redirect('/login');
    });

function checkUser(user) {
    return user && isTrueUser(user);
}

function isTrueUser(user) {
    return user.username === USERNAME && user.password === PASSWORD;
}

async function getAds(params, id, index) {
    let fileName = '';

    console.log(`Started parse for ${params.uri[index]}`);

    await new Promise(resolve => {
        request(
            {
                method: 'GET',
                uri: params.uri[index],
                headers: [ headers.CATEGORIES_HEADERS ]
            },
            async (error, response, body) => {
                if (error) {
                    return console.error('error with process form: ', error);
                }

                io.to(id).emit('start');

                fileName = await processAdsAndStartCalculatePurchaseCount(body, params, id);

                resolve('Success!');
            }
        );
    });

    if (params.uri[index + 1]) getAds(params, id, index + 1);
}

async function processAdsAndStartCalculatePurchaseCount(body, params, id) {
    let links
      , linksPerPage
      , otherPages
      , histories = []
      , fileName = `./results/${Date.now()}_result.csv`;

    links = await getLinksFromHTML(body);

    otherPages = await getOtherPages(body, params.maxCount);

    linksPerPage = links.length;

    if (params.maxCount && params.maxCount > linksPerPage) {
        otherPages = await getPagesForProcess(params.maxCount, linksPerPage, otherPages);
    }

    links = await getLinksFromOtherPages(otherPages, links);

    if (params.maxCount) {
        links = links.filter(link => link);

        links = _.slice(links, 0, params.maxCount);
    }

    links = links.filter(link => {
        let isProductWithZeroPurchases = link.order === ' Заказы (0)';

        if (isProductWithZeroPurchases) {
            histories.push({
                link: link.link,
                count: {
                    count: 0,
                    countFromRussia: 0
                }
            });
        }

        return !isProductWithZeroPurchases;
    }).map(link => link.link);

    io.to(id).emit('links', links.length + histories.length);

    await new Promise(async resolve => {
        let history = await getPurchaseHistories(links, params.dates, id);

        resolve(history);
    })
        .then(history => {
            io.to(id).emit('finish', histories.length);

            fs.appendFileSync(fileName,'Link,Purchases from the Russia,Total');

            history = _.concat(histories, _.compact(history));

            history.forEach(item => {
                fs.appendFileSync(
                    fileName,
                    '\n' +
                    `https:${item.link.slice(0, item.link.indexOf('?'))}, ${item.count.countFromRussia}, ${item.count.count}`
                );
            });

            io.to(id).emit('file', fileName);
        });
}

async function getLinksFromHTML(html) {
    let $ = cheerio.load(html)
      , links;

    links = await getProductLinks($);

    if ($('#hs-below-list-items script').html() !== null) {
        $ = cheerio.load($('#hs-below-list-items script').html());

        links = _.concat(links, await getProductLinks($));
    }

    return links;
}

async function getProductLinks($) {
    return _.map($('a.product'), link => {
        return {
            link: link.attribs.href,
            order: $('a[href="' + link.attribs.href + '"]').parent().parent().find('.order-num-a em').text()
        }
    });
}

async function getOtherPages(html, maxCount) {
    const $ = cheerio.load(html)
        , href = $('.ui-pagination-navi.util-left > a')[0].attribs.href;

    let lastPage = parseInt($('#pagination-max').text())
      , pages = [];

    if (maxCount && lastPage > (_.ceil(maxCount / 44) + 2)) {
        lastPage = _.ceil(maxCount / 44) + 2;
    }

    if (lastPage > 1) {
        for (let i = 2; i <= lastPage; i++) {
            pages.push(href.replace('2.html', `${i}.html`));
        }
    }

    return pages;
}

async function getPagesForProcess(maxCount, linksPerPage, otherPages) {
    let pagesForProcess = _.floor(maxCount / linksPerPage);

    return _.slice(otherPages, 0, pagesForProcess);
}

async function getLinksFromOtherPages(otherPages, links) {
    let promises = []
      , newLinks = [];

    otherPages.forEach(async (uri, index) => {
        promises.push(
            new Promise(async resolve => {
                delay(100 * index)
                    .then(async () => {
                        let links = await getLinksByPage(uri);

                        resolve(links);
                    });
            })
        );
    });

    await Promise.all(promises).then(values => {
        newLinks = _.concat(links, _.flatten(values))
    });

    return newLinks;
}

async function getLinksByPage(uri) {
    let links = [];

    if (uri) {
        await new Promise(async resolve => {
            try {
                request(
                    {
                        method: 'GET',
                        uri: `https:${uri.trim()}`,
                        headers: [ headers.CATEGORIES_HEADERS ]
                    },
                    await (async (error, response, body) => {
                        if (error) return;

                        resolve(body);
                    })
                );
            } catch (error) {
                //
            }

        })
            .then(async html => links = await getLinksFromHTML(html));
    }

    return links;
}

async function getPurchaseHistories(links, dates, id) {
    let promises = []
      , histories = [];

    links.forEach((link, index) => {
        if (link) {
            promises.push(new Promise(async resolve => {
                let count = await getPurchaseHistoryByLink(link, dates, index);

                io.to(id).emit('process', 1);

                resolve({ link, count });
            }));
        }
    });

    await Promise.all(promises).then(values => histories = values);

    return histories;
}

async function getPurchaseHistoryByLink(link, dates, index) {
    let purchases = { count: 0, countFromRussia: 0 };

    if (link) {
        await new Promise(async resolve => {
            await delay(TIME_TO_PROCESS_ONE_PRODUCT_IN_MILLISECONDS * index)
                .then(async () => {
                    try {
                        request(
                            {
                                method: 'GET',
                                uri: `https:${link.trim()}`,
                                headers: [ headers.PRODUCT_HEADERS ]
                            },
                            async (error, response, body) => {
                                if (error) resolve('');

                                resolve(body);
                            }
                        );
                    } catch (error) {
                        //
                    }
                });
        })
            .then(async html => {
                let productId = await getProductIdFromHTML(html);

                if (productId) {
                    purchases = await getPurchaseHistoryCount(productId, dates);
                }
            });
    }

    return purchases;
}

async function getProductIdFromHTML(html) {
    const $ = cheerio.load(html);

    return $('input[name=objectId][type=hidden]').val();
}

async function getPurchaseHistoryCount(productId, dates) {
    let promises = []
      , countFromRussia = 0
      , count = 0
      , afterBorder = ''
      , beforeBorder = ''
      , totalPages = 1
      , allRecords = [];

    [ afterBorder, beforeBorder ] = [ moment(dates['after'], 'D MMMM, YYYY'), moment(dates['before'], 'D MMMM, YYYY')];

    await new Promise(resolve => {
        try {

            request(
                {
                    method: 'GET',
                    uri: urls.getPurchaseHistoryUrl(productId),
                    headers: [ headers.PURCHASE_HISTORY_HEADERS ]
                },
                async (error, response, body) => {
                    if (error) resolve('time out of server');

                    if (body !== undefined) {
                        body = JSON.parse(body);

                        allRecords = body.records;

                        totalPages = parseInt(body.page.total) > 50 ? 50 : parseInt(body.page.total);

                        for (let page = 2; page <= parseInt(body.page.total); page++) {
                            promises.push(
                                new Promise(async resolve => {
                                    delay(10 * page)
                                        .then(async () => {
                                            let purchases = await getPurchaseHistoryByPage(productId, page);

                                            resolve(purchases);
                                        });
                                }));
                        }

                        await Promise.all(promises)
                            .then(async values => {
                                values.forEach(purchase => {
                                    if (purchase.records !== undefined) {
                                        allRecords = _.concat(allRecords, purchase.records);
                                    }
                                });

                                allRecords = _.uniqBy(allRecords, 'id');

                                if (afterBorder.isValid() || beforeBorder.isValid()) {
                                    allRecords = allRecords.filter(record => {
                                        let date = moment(record.date, 'DD MMM YYYY hh:mm');

                                        return date.isValid()
                                            && (!afterBorder.isValid() || date.isSameOrAfter(afterBorder))
                                            && (!beforeBorder.isValid() || date.isSameOrBefore(beforeBorder));
                                    });
                                }

                                countFromRussia += await calculatePurchaseCountFromRussia(allRecords);
                                count += await calculatePurchaseCount(allRecords);
                            });
                    }

                    resolve('Success!')
                }
            );
        } catch (error) {
            //
        }
    });

    return { countFromRussia, count };
}

async function calculatePurchaseCountFromRussia(records) {
    let count = 0;

    records.forEach(purchase => {
        if (purchase && purchase.countryCode === 'ru') {
            count += parseInt(purchase.quantity);
        }
    });

    return count;
}

async function calculatePurchaseCount(records) {
    let count = 0;

    records.forEach(purchase => {
        if (purchase) {
            count += parseInt(purchase.quantity);
        }
    });

    return count;
}

async function getPurchaseHistoryByPage(productId, page) {
    let purchases = [];

    await new Promise(async resolve => {
        try {
            request(
                {
                    method: 'GET',
                    uri: urls.getPurchaseHistoryUrl(productId, page),
                    headers: [ headers.PURCHASE_HISTORY_HEADERS ]
                },
                async (error, response, body) => {
                    if (body) purchases = JSON.parse(body);

                    resolve('Success!');
                }
            );
        } catch (error) {
            //
        }
    });

    return purchases;
}

server.listen(PORT, () => {
    console.log(`Parser listen port ${PORT} and ready to work`);
});

io.on('connection', client => {
    client.on('join', () => {
        client.join(client.id);
    });
});